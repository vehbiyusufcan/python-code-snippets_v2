from icecream import ic
from collections import Counter
from os import path
import time
import re
import sys
#Merge two dictionaries
def merge_two_dictionaries():
    dictionary1 = {"name": "joy", "age": 25}
    dictionary2 = {"name": "Joy", "city": "New York"}
    merged_dict = {**dictionary1,**dictionary2}
    ic(merged_dict)

#Chain comparison
def chain_comprasion():
    a=5
    ic(2 < a < 8)
    ic(1 == a < 3)

#Print a string N times 
def print_a_string_n_times():
    n = 5
    string = "Yusuf"

    ic(string*n)

#Check if a file exists
def check_if_a_file_exist():
    ic(path.exists("README.md"))

#Retrieve the last element of a list
def retrive_the_last_element_of_alist():
    myt_list = ["yusuf","can","mihrimah","aras","arya"]

    #method 1
    last_element = myt_list[len(myt_list)-1]
    ic(last_element)

    #method 2
    last_element = myt_list[-1]
    ic(last_element)

    #method 3
    last_element = myt_list.pop()
    ic(last_element)

#list Comprehension
def list_comprehension(input_string):
    return [vowel for vowel in input_string if vowel in 'aeiou']

#calculate code execution time
def calculate_code_execution_time():
    start_time = time.time()
    total = 0

    for i in range(10):
        total += i
    ic(total)

    end_time = time.time()
    time_taken = end_time - start_time
    ic(time_taken)

#Find element with most occurance
def find_element_with_most_occurance(input_list):
    return max(set(input_list), key=input_list.count)

#Convert Two list into a dictionary
def convert_two_list_into_a_dictionary(key,values):
    return dict(zip(key,values))

#Error Handling
def error_handling():
    a,b = 1,0
    try:
        ic(a/b)
    except ZeroDivisionError:
        ic("Can not divide by zero")
    finally:
        ic("Execution finally block")

#Reverse String
def reverse_string():
    str = "yusuf can"
    ic(str[::-1])

#Combaining a list of strings into a single string
def combaining_a_list_of_strings_into_a_single_string():
    my_list=["Hello","I","came","today!!"]
    new_string = " ".join(my_list)
    ic(new_string)

#Get default valeus for missing keys
def get_default_values_for_missing_keys():
    dict = {1:'one',2:'two',4:'four'}

    ic(dict.get(3,'three'))
    ic(dict)

#swap two values without an extra variable
def swap_two_values_without_an_extra_variable():
    a,b=5,10

    #method 1
    a,b=b,a
    
    #method 2
    def swap(a,b):
        return b,a
    ic(swap(a,b))

#regular expression
def regular_expression():
    text = "The rain in Turkey"
    result = re.search("rain",text)
    ic(True if result else False)

#Palindrome
def palindrome(input_string):
    return input_string == input_string[::-1]

#Filter Value
def filter_value():
    my_list = [1,2,3,4,5,6,7,8,9,10]

    result = filter(lambda x:x % 2 !=0 , my_list)
    ic(list(result))

#Anagrams
def anagrams(string1,string2):
    return Counter(string1) == Counter(string2)

#Memory usage of variable
def memory_usage_of_variable():
    var1 = 15
    my_list = [1,2,3,4,5]
    my_tuple = (1,2,3,4)

    my_for = [var1,my_list,my_tuple]
    for i in my_for:
        ic(i ,sys.getsizeof(i))

#chained function call
def chained_function_call():
    def add(a,b):
        return a+b
    
    def substract(a,b):
        return a-b

    a,b=5,10
    ic((add if b>a else substract)(a,b))

#get the unique id of an object
def get_the_unique_id_of_an_object():
    num = 15
    name = "yusuf"
    my_list = [1,2,3]

    my_for = [num,name,my_list]

    for i in my_for:
        ic(i,id(i))

#Remove duplicates from a list
def remove_duplicates_from_a_list(input_list):
    return list(set(input_list))
    

if __name__ == "__main__":
    merge_two_dictionaries()
    chain_comprasion()
    print_a_string_n_times()
    check_if_a_file_exist()
    retrive_the_last_element_of_alist()
    ic(list_comprehension("this is some random string"))
    calculate_code_execution_time()
    ic(find_element_with_most_occurance([1,1,2,2,2,3,4,5,6,]))
    list1 = [1,2,3]
    list2 = ['one','two','three']
    ic(convert_two_list_into_a_dictionary(list1,list2))
    error_handling()
    reverse_string()
    combaining_a_list_of_strings_into_a_single_string()
    get_default_values_for_missing_keys()
    swap_two_values_without_an_extra_variable()
    regular_expression()
    ic(palindrome("hannah"))
    filter_value()
    ic(anagrams("raki","irak"))
    memory_usage_of_variable()
    chained_function_call()
    get_the_unique_id_of_an_object()
    ic(remove_duplicates_from_a_list([1,2,3,3,4,'John', 'Ana', 'Mark', 'John']))
